//
//  RoundedView.swift
//  BitOasis
//
//  Created by Ramiz Rafiq on 11/17/18.
//  Copyright © 2018 Ramiz Rafiq. All rights reserved.
//

import UIKit

class RoundedView: UIView {

    @IBInspectable public var cornerRadius:CGFloat = 2.0
        {
        didSet{
            
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
        }
    }
    override func layoutSubviews() {
        
        let layer = self.layer
        layer.masksToBounds = false
        layer.shadowOffset = CGSize.zero
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 10.0
        layer.shadowOpacity = 0.25
        
        
        let backgroundCGColor = self.backgroundColor?.cgColor
        self.backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }


}
