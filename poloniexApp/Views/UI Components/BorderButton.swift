//
//  BorderButton.swift
//  BitOasis
//
//  Created by Ramiz Rafiq on 11/18/18.
//  Copyright © 2018 Ramiz Rafiq. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    @IBInspectable public var borderWidth:CGFloat = 1.0
        {
        didSet{
            
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable public var borderColor:UIColor = UIColor.black
        {
        didSet{
            
            layer.borderColor = borderColor.cgColor
        }
    }


}
