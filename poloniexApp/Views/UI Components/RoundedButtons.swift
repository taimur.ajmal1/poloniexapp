//
//  RoundedButtons.swift
//  BitOasis
//
//  Created by Ramiz Rafiq on 11/17/18.
//  Copyright © 2018 Ramiz Rafiq. All rights reserved.
//

import UIKit

class RoundedButtons: UIButton {
    
    @IBInspectable public var cornerRadius:CGFloat = 2.0
    {
        didSet{
            
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
        }
    }
    override open var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? UIColor.getHexaColors(hex: "0xe1e1e1") : UIColor.getHexaColors(hex: "0x2F85E9")
        }
    }
    

}
