//
//  TradeWrapper.swift
//  poloniexApp
//
//  Created by Taimur Ajmal on 12/9/18.
//  Copyright © 2018 Taimur Ajmal. All rights reserved.
//

import UIKit
class TradeWrapper: NSObject {

  var channelId:NSNumber? = nil
  var tradeModel:TradeData? = nil

  required init?(map: [Any]) {

    if map.count > 2
    {
      if let innerMap:NSArray = map[2] as? NSArray
      {
        if let receivingChannelId = map[0] as? NSNumber
        {
          channelId = receivingChannelId
        }

        tradeModel = TradeData()

        tradeModel?.currencyPairId = innerMap[0] as? NSNumber
        tradeModel?.lastTradePrice =  Utilities.convertStringToNumber(string: innerMap[1] as? String)
        tradeModel?.lowestAsk = Utilities.convertStringToNumber(string: innerMap[2] as? String)
        tradeModel?.highestBid = Utilities.convertStringToNumber(string: innerMap[3] as? String)
        tradeModel?.percentageChangeInLast24Hour = Utilities.convertStringToNumber(string: innerMap[4] as? String)
        tradeModel?.baseCurrencyVolumeInLast24Hour = Utilities.convertStringToNumber(string: innerMap[5] as? String)
        tradeModel?.quoteCurrencyVolumeInLast24hour = Utilities.convertStringToNumber(string: innerMap[6] as? String )
        tradeModel?.isFrozen = innerMap[7] as? Bool
        tradeModel?.highestTradePriceInLast24Hour = Utilities.convertStringToNumber(string: innerMap[8] as? String )
        tradeModel?.lowestTradePriceInLast24Hour = Utilities.convertStringToNumber(string: innerMap[9] as? String)

      }
    }

  }

}

