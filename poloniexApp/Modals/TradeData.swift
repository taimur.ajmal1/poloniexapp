//
//  TradeData.swift
//  poloniexApp
//
//  Created by Taimur Ajmal on 12/9/18.
//  Copyright © 2018 Taimur Ajmal. All rights reserved.
//

import Foundation

class TradeData: NSObject {

  var currencyPairId:NSNumber? = nil
  var lastTradePrice:NSNumber? = nil
  var lowestAsk:NSNumber? = nil
  var highestBid:NSNumber? = nil
  var percentageChangeInLast24Hour:NSNumber? = nil
  var baseCurrencyVolumeInLast24Hour:NSNumber? = nil
  var quoteCurrencyVolumeInLast24hour:NSNumber? = nil
  var isFrozen:Bool? = nil
  var highestTradePriceInLast24Hour:NSNumber? = nil
  var lowestTradePriceInLast24Hour:NSNumber? = nil
}
