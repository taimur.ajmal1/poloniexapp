//
//  User.swift
//  poloniexApp
//
//  Created by Taimur Ajmal on 12/9/18.
//  Copyright © 2018 Taimur Ajmal. All rights reserved.
//

import UIKit
import RealmSwift

class User: Object {

  @objc dynamic var email = ""
  @objc dynamic var mobile:NSNumber = 0
  @objc dynamic var password = ""
}
