//
//  LoggedUser.swift
//  poloniexApp
//
//  Created by Taimur Ajmal on 12/9/18.
//  Copyright © 2018 Taimur Ajmal. All rights reserved.
//

import RealmSwift

class LoggedUser: Object {

  @objc dynamic var loggedUser:User?

}
