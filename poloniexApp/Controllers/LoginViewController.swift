//
//  LoginViewController.swift
//  poloniexApp
//
//  Created by Taimur Ajmal on 12/8/18.
//  Copyright © 2018 Taimur Ajmal. All rights reserved.
//

import UIKit
import RealmSwift

class LoginViewController: UIViewController {

  @IBOutlet weak var txtfEmailAddress: CampaignTextfield!
  @IBOutlet weak var txtfPassword: CampaignTextfield!

  override func viewDidLoad() {
    super.viewDidLoad()
    txtfEmailAddress.text = "taimur.ajmal@gmail.com"
    txtfPassword.text = "123456"
    // Do any additional setup after loading the view.
  }
  @IBAction func showMobileValidation() {
    if checkValidation() && checkNewUser() {
      openMobileVerification()
    }
  }

  func checkNewUser() -> Bool {
    let realmObj = try! Realm()
    let users = realmObj.objects(User.self).filter("email == %@",txtfEmailAddress.text!).first
    if users == nil {
      return true
    }

    showAlert(title:"Error", text: "A user already exists with this email")
    return false
  }

  func validateUser() -> Bool {

    let realmObj = try! Realm()
    let userObj = realmObj.objects(User.self).filter("email == %@ AND password == %@",txtfEmailAddress.text!,txtfPassword.text!).first
    if userObj == nil {
      showAlert(title:"Error", text: "There is no user with this credentials")
      return false
    }

    let loggedUserObj = LoggedUser()
    loggedUserObj.loggedUser = userObj
    try! realmObj.write {
      realmObj.add(loggedUserObj)
    }

    return true
  }
  func openMobileVerification() {

    let userModel = User()
    userModel.email = txtfEmailAddress.text!
    userModel.password = txtfPassword.text!

    let mobileValidationView = self.storyboard?.instantiateViewController(withIdentifier: "MobileValidation") as! MobileValidationVC
    mobileValidationView.userObj = userModel
    self.navigationController?.pushViewController(mobileValidationView, animated: true)
  }

  @IBAction func login() {
    if checkValidation() {
      if validateUser() {
        openTradingScreen()
      }
    }
  }

  func openTradingScreen() {
    let tradingView = self.storyboard?.instantiateViewController(withIdentifier: "TradingView")
    self.navigationController?.pushViewController(tradingView!, animated: true)
  }

  func checkValidation() -> Bool {

    var resultValidation = true
    if !Utilities.isValidEmail(email: txtfEmailAddress.text!) {
      resultValidation = false
      showAlert(title:"Error", text: "Please enter a valid email address")
    }
    else if !Utilities.isValidPassword(password: txtfPassword.text!) {
      resultValidation = false
      showAlert(title:"Error", text: "Password must be greater than 6 characters")
    }

    return resultValidation
  }

  func showAlert(title: String, text: String) {
    let alertController = UIAlertController(title: "Some Error",
                                            message: text,
                                            preferredStyle: .alert)

    let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
    alertController.addAction(defaultAction)

    self.present(alertController, animated: true, completion: nil)
  }


}

