//
//  MobileValidationVC.swift
//  poloniexApp
//
//  Created by Taimur Ajmal on 12/9/18.
//  Copyright © 2018 Taimur Ajmal. All rights reserved.
//

import UIKit
import RealmSwift

class MobileValidationVC: UIViewController {

  @IBOutlet weak var txtOTP: UITextField!
  @IBOutlet weak var lblTitleOtp: UILabel!
  @IBOutlet weak var txtMobileNumber: CampaignTextfield!
  var userObj:User = User()

  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.

    let viewForDone = UIToolbar()
    viewForDone.sizeToFit()
    let btnDoneOnKeyboard = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneBtnFromKeyboardClicked))
    viewForDone.items = [btnDoneOnKeyboard]
    btnDoneOnKeyboard.tintColor = UIColor.getHexaColors(hex: "0x4a4a4a")
    txtMobileNumber.inputAccessoryView = viewForDone

    txtMobileNumber.delegate = self
    txtOTP.delegate = self
  }
  
  @objc func doneBtnFromKeyboardClicked() {
    if checkUnique(digits: txtMobileNumber.text!)
    {
      otpVisiblity(show: 1)
      txtOTP.becomeFirstResponder()
    }
  }

  func checkUnique(digits: String) -> Bool {
    let realmObj = try! Realm()
    if let mobileDigits = Int(digits) {
      let users = realmObj.objects(User.self).filter("mobile == %d",mobileDigits).first
      if users == nil {
        return true
      }
      else {
        showAlert(text: "A mobile number already exists with this email")
      }
    }
    return false
  }

  func showAlert(text:String) {
    Utilities.addNotificationView(text: text, color: UIColor.red, frame: CGRect(x:0,y:0,width:self.view.frame.size.width,height:100))
  }

  func verifyOTP() {
    if txtOTP.text == "1234"{
      lblTitleOtp.text = "Registration in progress ..."
      setupUser()
      openTradingScreen()
    }
    else {
      lblTitleOtp.text = "Invalid OTP, please try again"
    }
  }

  func setupUser() {
    if let mobileInt = Int(txtMobileNumber.text!) {
      userObj.mobile = NSNumber.init(value: Int(mobileInt))

      let realmObj = try! Realm()
      try! realmObj.write {
        realmObj.add(userObj)
      }
      let loggedUserObj = LoggedUser()
      loggedUserObj.loggedUser = userObj
      try! realmObj.write {
        realmObj.add(loggedUserObj)
      }
    }
  }

  @IBAction func goBack() {
    self.navigationController?.popViewController(animated: true)
  }

  func openTradingScreen() {
    let tradingView = self.storyboard?.instantiateViewController(withIdentifier: "TradingView")
    self.navigationController?.pushViewController(tradingView!, animated: true)
  }

  func otpVisiblity(show:CGFloat) {
    lblTitleOtp.alpha = show
    txtOTP.alpha = show
  }

}
extension MobileValidationVC: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    if textField == txtMobileNumber {
      doneBtnFromKeyboardClicked()
    } else if textField == txtOTP {
      verifyOTP()
    }
    return true
  }

  func textFieldDidBeginEditing(_ textField: UITextField) {

    if let _:CampaignTextfield = textField as? CampaignTextfield
    {
      otpVisiblity(show: 0)
    }

  }
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

    let  char = string.cString(using: String.Encoding.utf8)!
    let isBackSpace = strcmp(char, "\\b")

    if isBackSpace == -92 {
      if textField != txtOTP && lblTitleOtp.alpha == 1
      {
        otpVisiblity(show: 0)
      }
      return true
    }

    if let txtMobile = textField as? CampaignTextfield {
      if (txtMobile.text!.count == 13) || (txtMobile.text!.count == 9 && txtMobile.text!.first != "0")
      {
        return false
      }
    }

    let charString = txtOTP.text! + string
    if charString.count == 4 {
      txtOTP.text = charString
      verifyOTP()
      return false
    }
    else if charString.count >= 4 {
      return false
    }

    return true
  }
}

