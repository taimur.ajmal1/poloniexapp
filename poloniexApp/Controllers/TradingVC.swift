//
//  TradingVC.swift
//  poloniexApp
//
//  Created by Taimur Ajmal on 12/9/18.
//  Copyright © 2018 Taimur Ajmal. All rights reserved.
//

import UIKit
import Starscream
import RealmSwift

class TradingVC: UIViewController {

  @IBOutlet weak var viewPlain:UIView!
  @IBOutlet weak var viewToggle:UIView!

  // View One Value
  @IBOutlet weak var s1LastTradePrice:UILabel!
  @IBOutlet weak var s1percentChangeInLast24Hour:UILabel!

  @IBOutlet weak var s2HighestTradePrice:UILabel!
  @IBOutlet weak var s2LowestTradePrice:UILabel!

  @IBOutlet weak var imgIndicator:UIImageView!


  var socket:WebSocket? = nil

  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.
    showDefault(value: true)
    socket = WebSocket(url: URL(string: "wss://api2.poloniex.com")!, protocols: ["ticker"])
    socket?.delegate = self
    socket?.connect()

  }
  @IBAction func showDefault(sender:UISwitch)
  {
    showDefault(value: !sender.isOn)
  }
  deinit {
    socket?.disconnect(forceTimeout: 0)
    socket?.delegate = nil
  }
  func showDefault(value:Bool)
  {
    UIView.animate(withDuration: 0.5, animations: {

      self.viewPlain.alpha = CGFloat(truncating: NSNumber.init(value: value))
      self.viewToggle.alpha = CGFloat(truncating: NSNumber.init(value: !value))

    })
  }
  @IBAction func processLogout()
  {
    let realmObj = try! Realm()
    let userSaved:Results<LoggedUser> = realmObj.objects(LoggedUser.self)
    try! realmObj.write {
      realmObj.delete(userSaved)
    }
    let loginView = self.storyboard?.instantiateViewController(withIdentifier: "loginView")
    self.navigationController?.pushViewController(loginView!, animated: true)

  }
  func setupValues(tradeDataObj:TradeData)
  {
    s1LastTradePrice.text = "Last Trade Price : " + (tradeDataObj.lastTradePrice?.stringValue)!
    s1percentChangeInLast24Hour.text = "Last 24 Hour Change : " + (tradeDataObj.percentageChangeInLast24Hour?.stringValue)!

    s2HighestTradePrice.text = "Highest Trade Price : " + (tradeDataObj.highestTradePriceInLast24Hour?.stringValue)!

    s2LowestTradePrice.text = "Lowest Trade Price : " + (tradeDataObj.lowestTradePriceInLast24Hour?.stringValue)!

    if let percentDoubleValue =  tradeDataObj.percentageChangeInLast24Hour?.doubleValue
    {
      if percentDoubleValue < 0
      {
        imgIndicator.image = UIImage.init(named: "ArrowDown")
      }
      else
      {
        imgIndicator.image = UIImage.init(named: "ArrowUp")
      }
    }

  }
  func sendSubscribeMessage()
  {
    let msg = "{\"command\":\"subscribe\",\"channel\":1002}"
    socket?.write(string: msg)
  }
  func setupData(rawText:String)
  {
    if let tradeDataObj = Utilities.convertRawToModel(rawText: rawText)
    {
      setupValues(tradeDataObj: tradeDataObj)
    }
  }
}
extension TradingVC : WebSocketDelegate {
  func websocketDidConnect(socket: WebSocketClient) {
    sendSubscribeMessage()
  }

  func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
    print("Disconnect")
  }

  func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
    setupData(rawText: text)
  }

  func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
    print("Received Data")
  }

}

