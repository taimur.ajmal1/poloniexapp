//
//  Color+Hexa.swift
//  poloniexApp
//
//  Created by Taimur Ajmal on 12/8/18.
//  Copyright © 2018 Taimur Ajmal. All rights reserved.
//

import UIKit

extension UIColor {

  class func getHexaColors(hex:String) -> UIColor {

    var rgbValue:UInt32 = 0
    Scanner(string: hex).scanHexInt32(&rgbValue)

    return UIColor(
      red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
      green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
      blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
      alpha: CGFloat(1.0)
    )
  }
}
