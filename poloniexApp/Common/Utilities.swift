//
//  Utilities.swift
//  poloniexApp
//
//  Created by Taimur Ajmal on 12/8/18.
//  Copyright © 2018 Taimur Ajmal. All rights reserved.
//

import Foundation
import RealmSwift

class Utilities: NSObject {

  class func isValidEmail(email:String) -> Bool {

    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)

    return emailTest.evaluate(with: email)
  }
  class func isValidPassword(password:String) -> Bool {

    if password.count >= 6
    {
      return true
    }

    return false
  }

  class func convertStringToNumber(string:String?) -> NSNumber {
    guard let valueNum = string else {
      return 0
    }
    if let dataValue = NumberFormatter().number(from: valueNum) {
      return dataValue
    }

    return 0
  }

  class func convertRawToModel(rawText:String) -> TradeData? {
    if let jsonDictionary = convertToDictionary(text: rawText) {
      let tradeWrapperObj = TradeWrapper.init(map: jsonDictionary)
      if let tradedataObj = tradeWrapperObj?.tradeModel
      {
        return tradedataObj
      }
    }

    return nil
  }
  class func isUserLogged() -> Bool {
    let realmObj = try! Realm()
    let users = realmObj.objects(LoggedUser.self)
    if users.count > 0 {
      return true
    }

    return false
  }

  class func convertToDictionary(text: String) -> [Any]? {
    if let data = text.data(using: .utf8) {
      do {
        return try JSONSerialization.jsonObject(with: data, options: []) as? [Any]
      } catch {
        print(error.localizedDescription)
      }
    }
    return nil
  }

  class func addNotificationView(text: String,color: UIColor,frame: CGRect) {

    removeNotification()

    let viewTop = UIView.init(frame: frame)
    viewTop.backgroundColor = color
    viewTop.tag = 19981

    let label = UILabel.init(frame: CGRect(x:0,y:43,width:frame.size.width,height:35))
    label.text = text
    label.textAlignment = .center
    label.numberOfLines = 0
    label.textColor = UIColor.white
    label.font = UIFont.init(name: "Rubik-Regular", size: 13.0)

    viewTop.addSubview(label)

    perform(#selector(removeNotification), with: nil, afterDelay: 2.0)

    let mainWindow = UIApplication.shared.keyWindow
    UIView.animate(withDuration: 0.5, delay: 0.5, options: UIView.AnimationOptions.curveEaseOut, animations: {
      mainWindow?.addSubview(viewTop)
    }, completion: nil)

  }

  @objc class func removeNotification() {
    let mainWindow = UIApplication.shared.keyWindow
    UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
      mainWindow?.viewWithTag(19981)?.removeFromSuperview()
    }, completion: nil)
  }

}
